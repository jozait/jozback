package com.jozback.domain.user;


import com.jozback.domain.BaseEntityWithAudit;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_user")
public class User extends BaseEntityWithAudit {
    private String email;

    private String firstname;

    private String lastname;

}