package com.jozback.domain.common.response;

public class JozbackApiWarning {
    public JozbackApiWarning() {
    }

    public JozbackApiWarning(final String code, final String message, final String field) {
        this.code = code;
        this.message = message;
        this.field = field;
    }

    private String code;

    private String message;

    private String field;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
