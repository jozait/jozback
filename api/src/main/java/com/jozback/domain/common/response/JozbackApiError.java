package com.jozback.domain.common.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.http.HttpStatus;

@JsonIgnoreProperties({"stackTrace", "cause", "suppressed", "localizedMessage"})
public class JozbackApiError extends RuntimeException {

    public static final JozbackApiError AUTHENTICATION_FAILED = new JozbackApiError("A0001", "Authentication failed", HttpStatus.UNAUTHORIZED);
    public static final JozbackApiError FORBIDDEN = new JozbackApiError("A0002", "Forbidden action", HttpStatus.FORBIDDEN);


    public static final JozbackApiError USER_NOT_FOUND = new JozbackApiError("U0001", "User not found", HttpStatus.NOT_FOUND);
    public static final JozbackApiError USER_ALREADY_EXISTS = new JozbackApiError("U0002", "User already exists", HttpStatus.CONFLICT);

    public static final JozbackApiError CHILD_NOT_FOUND = new JozbackApiError("CH0001", "Child could not be found", HttpStatus.NOT_FOUND);
    public static final JozbackApiError CHILD_ALREADY_REGISTERED = new JozbackApiError("CH0002", "Child has already been registered to given activity", HttpStatus.CONFLICT);


    public static final JozbackApiError INVALID_REQUEST_ERROR = new JozbackApiError("IV0001", "Invalid input data", HttpStatus.BAD_REQUEST);

    public static final JozbackApiError PROFIL_ACCESS_DENIED = new JozbackApiError("P0001", "Access denied", HttpStatus.FORBIDDEN);


    public static final JozbackApiError ACTIVITY_NOT_FOUND = new JozbackApiError("AC0001", "Activity could not be found", HttpStatus.NOT_FOUND);

    public static final JozbackApiError DOCUMENT_URL_INVALID = new JozbackApiError("D0001", "Document url is invalid", HttpStatus.BAD_REQUEST);

    public static final String GENERIC_ERROR_CODE = "G0001";
    public static final JozbackApiError ROLE_NOT_FOUND = new JozbackApiError("R0001", "Role could not be found", HttpStatus.NOT_FOUND);

    private String code;
    private String message;
    private String detail;
    private HttpStatus httpStatus;

    public JozbackApiError() {
    }

    public JozbackApiError(final String code, final String message) {
        this.code = code;
        this.message = message;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }

    public JozbackApiError(final String code, final String message, final HttpStatus httpStatus) {
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public JozbackApiError addDetail(String detail) {
        this.setDetail(detail);
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(final String detail) {
        this.detail = detail;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(final HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
