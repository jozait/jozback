package com.jozback.domain.common.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"stackTrace", "cause", "suppressed", "localizedMessage"})
public class JozbackApiResponseError extends RuntimeException {

    private JozbackApiResponse apiResponse;

    private JozbackApiError apiError;

    public JozbackApiResponseError(JozbackApiResponse response) {
        super();
        this.apiResponse = response;
    }

    public JozbackApiResponseError(JozbackApiError error) {
        super(error.getMessage());
        this.apiError = error;
    }

    public JozbackApiResponse getResponse() {
        return apiResponse;
    }

    public void setResponse(JozbackApiResponse response) {
        this.apiResponse = response;
    }

    public JozbackApiError getApiError() {
        return apiError;
    }

    public void setApiError(JozbackApiError apiError) {
        this.apiError = apiError;
    }
}
