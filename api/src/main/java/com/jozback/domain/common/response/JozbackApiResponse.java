package com.jozback.domain.common.response;

import java.util.ArrayList;
import java.util.List;

public class JozbackApiResponse<T> {
    private T content;

    private List<JozbackApiWarning> warnings = new ArrayList<>();

    private List<JozbackApiError> errors = new ArrayList<>();

    public JozbackApiResponse() {
    }

    public JozbackApiResponse(final T content, final List<JozbackApiWarning> warnings, final List<JozbackApiError> errors) {
        this.content = content;
        this.warnings = warnings;
        this.errors = errors;
    }

    public T getContent() {
        return content;
    }

    public void setContent(final T content) {
        this.content = content;
    }

    public List<JozbackApiWarning> getWarnings() {
        return warnings;
    }

    public void setWarnings(final List<JozbackApiWarning> warnings) {
        this.warnings = warnings;
    }

    public List<JozbackApiError> getErrors() {
        return errors;
    }

    public void setErrors(final List<JozbackApiError> errors) {
        this.errors = errors;
    }
}
