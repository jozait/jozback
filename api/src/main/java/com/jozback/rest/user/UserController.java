package com.jozback.rest.user;

import com.jozback.domain.user.User;
import com.jozback.repository.user.UserRepository;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("${api.base.url}/users")
@SecurityRequirement(name = "JOZBACK API")
public class UserController {
    @Autowired
    private UserRepository userRepository;


    @RequestMapping(value = "/get-all", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUsers() {

        List<User> users = this.userRepository.findAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}
