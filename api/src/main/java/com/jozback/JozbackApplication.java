package com.jozback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JozbackApplication {

    public static void main(String[] args) {
        SpringApplication.run(JozbackApplication.class, args);
    }

}
