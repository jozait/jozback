//package com.jozback.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.core.io.Resource;
//import org.springframework.web.servlet.config.annotation.*;
//import org.springframework.web.servlet.resource.PathResourceResolver;
//import org.springframework.web.servlet.view.InternalResourceViewResolver;
//
//import java.io.IOException;
//
//@Configuration
//@EnableWebMvc
//public class WebConfig implements WebMvcConfigurer {
////    @Override
////    public void addCorsMappings(final CorsRegistry registry) {
////        registry
////                .addMapping("/jozback-api/**")
////                .allowedOrigins("*")
////                .allowedMethods("GET", "PUT", "POST", "DELETE");
////    }
////
////    @Bean
////    public InternalResourceViewResolver defaultViewResolver() {
////        return new InternalResourceViewResolver();
////    }
//
//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//        registry.addResourceHandler("/**/*")
//                .addResourceLocations("classpath:/static/")
//                .resourceChain(true)
//                .addResolver(new PathResourceResolver() {
//                    @Override
//                    protected Resource getResource(final String resourcePath, final Resource location) throws IOException {
//                        Resource requestedResource = location.createRelative(resourcePath);
//                        return requestedResource.exists() && requestedResource.isReadable() ? requestedResource : new ClassPathResource("/static/index.html");
//                    }
//                });
//    }
//}
