create table role
(
    id      bigserial
        constraint pk_role primary key,
    version int  not null,
    name    text not null
);

insert into role(name, version)
values ('ROLE_USER', 1);
insert into role(name, version)
values ('ROLE_ADMIN', 1);

create table jb_user
(
    id         bigserial
        constraint pk_user primary key,
    version    int       not null,
    email      text      not null unique,
    firstname  text      not null,
    lastname   text      not null,
    created_at timestamp not null,
    updated_at timestamp
);

create table user_role
(
    user_id bigint not null,
    role_id bigint not null,
    unique (user_id, role_id)
);

alter table user_role
    add constraint user_to_role_to_user_fk foreign key (user_id) references jb_user (id);
alter table user_role
    add constraint user_to_role_to_role_fk foreign key (role_id) references role (id);

create index user_role_user_id_idx on user_role (user_id);


