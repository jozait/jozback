# JOZBACK

## base de données

### création du user et de la base postgres

```
CREATE USER jozback WITH PASSWORD 'jozback';
CREATE DATABASE jozback;
GRANT CREATE ON DATABASE jozback TO jozback;
ALTER USER jozback WITH SUPERUSER
ALTER ROLE jozback SUPERUSER;
\connect jozback;
create schema jozback authorization jozback;
GRANT ALL PRIVILEGES ON DATABASE jozback to jozback;

```

### creation de la base de test postgres

Pas besoin pour le moment

```
CREATE DATABASE jozbacktest;
GRANT CREATE ON DATABASE jozbacktest TO jozback;
\connect jozbacktest;
create schema jozback authorization jozback;
```

### creation de l'extension et du schema pour l'audit de la base
```
CREATE EXTENSION IF NOT EXISTS hstore;
GRANT ALL PRIVILEGES ON ALL jozbacktest IN SCHEMA public TO jozback;
```

### effacer le contenu d'une base de données

```
\connect postgres
drop database jozback;
create database jozback;
```

NB : pour faire cette opération personne (ou aucune application) 
doit être connecté à cette database

## Dockerisation

### Builder les containers

Faire au préalable, un build totale de l'application
```
mvn clean install
```

Lancer les containers, à partir du répertoire de base du projet

```
docker-compose up
```

Arrêter les containers, à partir du répertoire de base du projet
```
docker-compose down
```

Pour lancer les containers et builder les images si nécessaire
```
docker-compose up --build
```

Builder les images
```
docker-compose build
```

Pour supprimer toutes les images et les containers en local
```
./docker-rm-all.sh
```

Envoyer les images sur DockerHub
```
docker-compose push
```

## déployer sur AWS de recette 

### exporter les images dockers

une fois avoir compiler le projet (mvn clean install) puis
builder les images (docker-compose build) il faut faire un export
des images du registry local vers AWS pour cela :

Exporter les images locales dans un fichier :

```
docker save poc-api_api:latest > poc-api_api.tar
docker save poc-api_database:latest > poc-api_database.tar
```

Copier les images sur la VM AWS

```
scp -i ~/.ssh/aws-joza-ext.pem *.tar ubuntu@ec2-35-180-156-137.eu-west-3.compute.amazonaws.com:~
```

Se logger sur la VM

```
ssh -i "~/.ssh/aws-joza-ext.pem" ubuntu@ec2-35-180-156-137.eu-west-3.compute.amazonaws.com
```

Aller dans le répertoire de travail :
```
cd app/poc-api/
```

Arrêter l'API
```
docker-compose down
```

Copier les images dans le registry de la VM AWS
```
docker load < ~/poc-api_api.tar
docker load < ~/poc-api_database.tar
```

Redémarrer les containers
```
nohup docker-compose up &
```

## Base de données

restaurer un dump
```
gunzip < last_dump.gz | psql -d jozback -U postgres
```
