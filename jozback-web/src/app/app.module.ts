import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LayoutComponent } from './components/layout/layout.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ButtonComponent } from './components/button/button.component';
import { ContentComponent } from './components/content/content.component';
import { UserBannerComponent } from './components/user-banner/user-banner.component';
import { ThemesComponent } from './pages/themes/themes.component';
import { HomeComponent } from './pages/home/home.component';
import { GridComponent } from './components/grid/grid.component';
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import { GridItemComponent } from './components/grid/grid-item/grid-item.component';
import {AuthModule} from "@auth0/auth0-angular";

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    SidebarComponent,
    LayoutComponent,
    NavbarComponent,
    ButtonComponent,
    ContentComponent,
    UserBannerComponent,
    ThemesComponent,
    HomeComponent,
    GridComponent,
    GridItemComponent,
  ],
  imports: [BrowserModule, AppRoutingModule,AuthModule.forRoot({
    domain: 'tawfiq.eu.auth0.com',
    clientId: 'TK95yT19Y7Y6NUQr2QGHNBvDpWwVciAT'
  }),],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
