import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  constructor() {
  }


  content = [
    {
      title: 'Java 17',
      imgUrl: 'https://upload.wikimedia.org/wikipedia/fr/thumb/2/2e/Java_Logo.svg/412px-Java_Logo.svg.png',
      description: 'Le langage Java reprend en grande partie la syntaxe du langage C++. Néanmoins, Java a été épuré des concepts les plus subtils du C++ et à la fois les plus déroutants, tels que les pointeurs et références, ou l’héritage multiple contourné par l’implémentation des interfaces. De même, depuis la version 8, l\'arrivée des interfaces fonctionnelles introduit l\'héritage multiple (sans la gestion des attributs) avec ses avantages et inconvénients tels que l\'héritage en diamant. Les concepteurs ont privilégié l’approche orientée objet de sorte qu’en Java, tout est objet à l’exception des types primitifs (nombres entiers, nombres à virgule flottante, etc.) qui ont cependant leurs variantes qui héritent de l\'objet Object (Integer, Float...).\n' +
        '\n' +
        'Java permet de développer des applications client-serveur. Côté client, les applets sont à l’origine de la notoriété du langage. C’est surtout côté serveur que Java s’est imposé dans le milieu de l’entreprise grâce aux servlets, le pendant serveur des applets, et plus récemment les JSP (JavaServer Pages) qui peuvent se substituer à PHP, ASP et ASP.NET.'
    },
    {
      title: 'Python 3',
      imgUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1200px-Python-logo-notext.svg.png',
      description: 'Python (prononcé /pi.tɔ̃/) est un langage de programmation interprété, multi-paradigme et multiplateformes. Il favorise la programmation impérative structurée, fonctionnelle et orientée objet. Il est doté d\'un typage dynamique fort, d\'une gestion automatique de la mémoire par ramasse-miettes et d\'un système de gestion d\'exceptions ; il est ainsi similaire à Perl, Ruby, Scheme, Smalltalk et Tcl.\n' +
        '\n' +
        'Le langage Python est placé sous une licence libre proche de la licence BSD4 et fonctionne sur la plupart des plates-formes informatiques, des smartphones aux ordinateurs centraux5, de Windows à Unix avec notamment GNU/Linux en passant par macOS, ou encore Android, iOS, et peut aussi être traduit en Java ou .NET. Il est conçu pour optimiser la productivité des programmeurs en offrant des outils de haut niveau et une syntaxe simple à utiliser.\n' +
        '\n' +
        'Il est également apprécié par certains pédagogues qui y trouvent un langage où la syntaxe, clairement séparée des mécanismes de bas niveau, permet une initiation aisée aux concepts de base de la programmation6.'
    },
    {
      title: 'Node.js',
      imgUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/1280px-Node.js_logo.svg.png',
      description: 'Node.js est une plateforme logicielle libre en JavaScript, orientée vers les applications réseau évènementielles hautement concurrentes qui doivent pouvoir monter en charge.\n' +
        '\n' +
        'Elle utilise la machine virtuelle V8, la librairie libuv pour sa boucle d\'évènements, et implémente sous licence MIT les spécifications CommonJS.\n' +
        '\n' +
        'Parmi les modules natifs de Node.js, on retrouve http qui permet le développement de serveur HTTP. Ce qui autorise, lors du déploiement de sites internet et d\'applications web développés avec Node.js, de ne pas installer et utiliser des serveurs webs tels que Nginx ou Apache.'
    },
    {
      title: 'AWS',
      imgUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1280px-Amazon_Web_Services_Logo.svg.png',
      description: 'Amazon Web Services (AWS) est une division du groupe américain de commerce électronique Amazon, spécialisée dans les services de cloud computing à la demande pour les entreprises et particuliers3.\n' +
        '\n' +
        'En 2020, AWS a généré 45 milliards de dollars des 386 milliards de dollars du chiffre d\'affaires (net sales) monde d\'Amazon4, soit 11.5%.\n' +
        '\n' +
        'Lancé officiellement en 2006 par Andy Jassy5, Amazon Web Services fournit des services en ligne à d\'autres sites internet ou applications clientes. La plupart d\'entre eux ne sont pas directement exposés à l\'utilisateur final, mais offrent des fonctionnalités que d\'autres développeurs peuvent utiliser à travers des API. En 2017, AWS propose plus de 90 services, comprenant le calcul, le stockage, le réseau, la base de données, l\'analyse de données, des services applicatifs, du déploiement, de la gestion de système, de la gestion d\'applications mobiles, des outils pour les développeurs et pour l\'Internet des objets. Les services les plus populaires sont Amazon Elastic Compute Cloud (EC2) et Amazon Simple Storage Service (S3).\n' +
        '\n'

    }
  ]

  ngOnInit(): void {
  }

}
