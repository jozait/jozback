import { Component, OnInit } from '@angular/core';
import { UiStoreService } from '../../services/ui-store/ui-store.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
    isSidebarVisible = false;

    constructor(private store: UiStoreService) {
        store.isSideBarVisible.subscribe((value) => {
            this.isSidebarVisible = value;
        });
    }

    ngOnInit(): void {}
}
