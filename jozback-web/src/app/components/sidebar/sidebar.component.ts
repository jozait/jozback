import {Component, OnInit} from '@angular/core';
import {SideBarItem} from '../../domain/sidebar-item';
import {UiStoreService} from '../../services/ui-store/ui-store.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  isSidebarVisible = false;

  constructor(private store: UiStoreService, private router: Router) {
    store.isSideBarVisible.subscribe((value) => {
      this.isSidebarVisible = value;
    });
  }

  items: SideBarItem[] = [
    {title: 'Accueil', id: 'home', selected: true},
    {title: 'Themes', id: 'themes', selected: false},
    {title: 'Team', id: 'team', selected: false},
    {title: 'Contact', id: 'contact', selected: false},
  ];

  ngOnInit(): void {
  }

  private navigateToRoute(route: string, itemId: string) {
    this.router.navigateByUrl(route).then(() => {
      this.items = this.items.map(el => {
        el.selected = el.id === itemId;
        return el;
      })
    })
  }

  onItemSelected(item: SideBarItem) {
    switch (item.id) {
      case 'themes':
        this.navigateToRoute("/themes", item.id);
        break;
      case 'home':
        this.navigateToRoute("/home", item.id);
        break;
      default:
        break;
    }
  }
}
