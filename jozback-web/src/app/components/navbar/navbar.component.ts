import {Component, OnInit} from '@angular/core';
import {UiStoreService} from '../../services/ui-store/ui-store.service';
import {AuthService} from "@auth0/auth0-angular";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  isSidebarVisible = false;
  pageTitle = "Accueil";

  constructor(private store: UiStoreService, public auth: AuthService) {
    store.isSideBarVisible.subscribe((value) => {
      this.isSidebarVisible = value;
    });
    store.pageTitle.subscribe(value => {
      this.pageTitle = value;
    })
  }

  async getAuthState() {
    return this.auth.isAuthenticated$;
  }

  ngOnInit(): void {
    console.log(this.getAuthState());
  }

  toggleSidebar = () => {
    this.store.setIsSidebarVisible(!this.isSidebarVisible);
  };
}
