
export interface BaseEntity {
    id: number;
    version: number;
}

export interface BaseEntityWithAudit extends BaseEntity {
    createdAt: Date;
    updatedAt: Date;
}

export interface User extends BaseEntityWithAudit {
}
