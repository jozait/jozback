export type SideBarItem = {
    title: string;
    id: string;
    selected: boolean;
};
