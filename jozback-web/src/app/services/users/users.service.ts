import { Injectable } from '@angular/core';
import { User } from '../../domain/user';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor() {}

  getAllUsers(): User[] {
    return [
      new User(1, 'Tawfiq', 'JAFFAR'),
      new User(2, 'Hamza', 'HASSEN'),
      new User(3, 'Berk', 'ONER'),
    ];
  }
}
