import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class UiStoreService {
    constructor() {}

    private _isSidebarVisible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    isSideBarVisible = this._isSidebarVisible$.asObservable();

    setIsSidebarVisible(isVisible: boolean) {
        this._isSidebarVisible$.next(isVisible);
    }

    private _pageTitle$: BehaviorSubject<string> = new BehaviorSubject<string>("Accueil");

    pageTitle = this._pageTitle$.asObservable();

    setPageTitle(value: string) {
      this._pageTitle$.next(value);
    }
}
