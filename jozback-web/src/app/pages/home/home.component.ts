import { Component, OnInit } from '@angular/core';
import {UiStoreService} from "../../services/ui-store/ui-store.service";
import {AuthService} from "@auth0/auth0-angular";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private uiStore : UiStoreService, public auth: AuthService) { }

  ngOnInit(): void {
    this.uiStore.setPageTitle("Accueil");
  }

}
