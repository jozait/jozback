import { Component, OnInit } from '@angular/core';
import {UiStoreService} from "../../services/ui-store/ui-store.service";

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.scss']
})
export class ThemesComponent implements OnInit {

  constructor(private uiStore : UiStoreService) { }

  ngOnInit(): void {
    this.uiStore.setPageTitle("Themes");
  }

}
